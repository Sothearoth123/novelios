//
//  HttpRequest.swift
//  MyNovel
//
//  Created by Thy sothearoth on 25/9/21.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

enum Result <T> {
    case success(T)
    case failure(String?)
}


class HttpRequest: NSObject {
    
    typealias RestResponse = ((_ response: JSON, _ responseCode: Int?, _ error: Error?) -> ())
    typealias RestResponseString = ((_ response: String, _ responseCode: Int?, _ error: Error?) -> ())
    
    static func post(url: String, parameters: Parameters = [:], callback: @escaping  (_ data: [String:Any]) -> Void ) {
        
        let endPoint =  url
        let headers = getHeader()
       // consoleLog("endPoint == \(endPoint)")
        print("endPoint == \(endPoint)")
        AF.request(endPoint, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .validate().response { response in
                handleCode(responseCode: response.response?.statusCode)
                switch response.result {
                case .success(let dataResult): // stores the json file in JSON variable
                    
                    print("dataResult == \(String(describing: dataResult))")
                    
                    guard let dataBody = try! JSONSerialization.jsonObject(with: dataResult!, options: .mutableContainers) as? [String:Any] else { return }
                    //print("Validation Successful with response JSON \(String(describing: dataBody))")
                    callback(dataBody)
                case .failure(let error):
                    print("Request failed with error \(error)")
                //  completion(.failure(error))
                }
            }
    }

    
    static func put(url: String, parameters: Parameters = [:], callback: @escaping  (_ data: [String:Any]) -> Void ) {
        
        let endPoint =  url
        let headers = getHeader()
        print("endPoint == \(endPoint)")
        
        AF.request(endPoint, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .validate().response { response in
                handleCode(responseCode: response.response?.statusCode)
                switch response.result {
                case .success(let dataResult): // stores the json file in JSON variable
                    
                    print("dataResult == \(String(describing: dataResult))")
                    
                    guard let dataBody = try! JSONSerialization.jsonObject(with: dataResult!, options: .mutableContainers) as? [String:Any] else { return }
                    //print("Validation Successful with response JSON \(String(describing: dataBody))")
                    callback(dataBody)
                case .failure(let error):
                    print("Request failed with error \(error)")
                //  completion(.failure(error))
                }
            }
    }
    class func onRequestPOST(url: String, param: [String:String], block: @escaping (_ data: [String:Any]) -> Void) {
        
        let headers = getHeader()
        let endPoint =  url
        
        print("endPoint == \(endPoint)")
        AF.request(endPoint, method: .post, parameters: param, encoder: JSONParameterEncoder.prettyPrinted, headers: headers).response { response in
           
            handleCode(responseCode: response.response?.statusCode)
            print("response.response?.statusCode == \(response.response?.statusCode)")
            switch response.result {
            case .success(let dataResult):
                
                print("dataResult == \(dataResult)")
                guard let dataBody = try! JSONSerialization.jsonObject(with: dataResult!, options: .mutableContainers) as? [String:Any] else { return }
                block(dataBody)
            case .failure(let err):
                print("EcrrorRequestPOST: \(err.localizedDescription)")
            
            }
        }
    }
    
    class func onRequestPUT(url: String, param: [String:String], block: @escaping (_ data: [String:Any]) -> Void) {
        
        let headers = getHeader()
        let endPoint =  url
        print("endPoint == \(endPoint)")
       
        AF.request(endPoint, method: .put, parameters: param, encoder: JSONParameterEncoder.prettyPrinted, headers: headers).response { response in
           
            handleCode(responseCode: response.response?.statusCode)
            print("response.response?.statusCode == \(String(describing: response.response?.statusCode))")
            switch response.result {
            case .success(let dataResult):
                
                print("dataResult == \(String(describing: dataResult))")
                guard let dataBody = try! JSONSerialization.jsonObject(with: dataResult!, options: .mutableContainers) as? [String:Any] else { return }
                block(dataBody)
            case .failure(let err):
                print("EcrrorRequestPOST: \(err.localizedDescription)")
            
            }
        }
    }
    class func onRequestDelete(url: String, param: [String:String], block: @escaping (_ data: [String:Any]) -> Void) {
        
        let headers = getHeader()
        let endPoint =  url
        print("endPoint == \(endPoint)")
       
        AF.request(endPoint, method: .delete, parameters: param, encoder: JSONParameterEncoder.prettyPrinted, headers: headers).response { response in
           
            handleCode(responseCode: response.response?.statusCode)
            print("response.response?.statusCode == \(String(describing: response.response?.statusCode))")
            switch response.result {
            case .success(let dataResult):
                
                print("dataResult == \(String(describing: dataResult))")
                guard let dataBody = try! JSONSerialization.jsonObject(with: dataResult!, options: .mutableContainers) as? [String:Any] else { return }
                block(dataBody)
            case .failure(let err):
                print("EcrrorRequestPOST: \(err.localizedDescription)")
            
            }
        }
    }
    
    class func onRequestGET(url: String, param: [String:String]?, block: @escaping (_ data: JSON) -> Void) {
        
        let headers = getHeader()
        let endPoint =  url
        
        print("endPoint == \(endPoint)")
        
        AF.request(endPoint, method: .get, encoding: JSONEncoding.default, headers: headers).response { response in
            handleCode(responseCode: response.response?.statusCode)
            switch response.result {
            case .success(let dataResult):
//                if dataResult == nil {
//                    block([String:Any]())
//                    return
//                }
//                print("dataResult === \(String(describing: dataResult))")
//                guard let dataBody = try! JSONSerialization.jsonObject(with: dataResult!, options: .mutableContainers) as? [String:Any] else { return }
//                    block(dataBody)
            
            block(JSON(dataResult))
    
            case .failure(let err):
                print("Error: \(err.localizedDescription)")
            }
        }
    }
        
    class func onRequestGETWithArrayData(url: String, param: [String:String]?, block: @escaping (_ arr: [[String:Any]] ) -> Void) {
        let headers = getHeader()
        let endPoint = url
        print("endPoint == \(endPoint)")
        AF.request(endPoint, method: .get, parameters: param, encoder: JSONParameterEncoder.prettyPrinted, headers: headers).response { response in
            handleCode(responseCode: response.response?.statusCode)
            switch response.result {
            case .success(let dataResult):
                if dataResult == nil {
                    block([[String:Any]]())
                } else {
                    guard let dataBody = try! JSONSerialization.jsonObject(with: dataResult!, options: .mutableContainers) as? [[String:Any]] else { return }
                    block(dataBody)
                }
            case .failure(let err):
                print("Error: \(err.localizedDescription)")
            }
        }
    }
    
    class func getHeader() -> HTTPHeaders? {
        let accessToken = UserDefaults.standard.string(forKey: KeyCache.accessToken) ?? ""
        print("accessToken ==:> \(accessToken)")
        let headers: HTTPHeaders = [
            "Authorization" : "bearer \(accessToken)"
        ]
        return headers
    }

    class  func handleCode(responseCode: Int?) {

        //consoleLog("internet \(InternetConnectionManager.shared.isInternetConnected)")

//        if !InternetConnectionManager.shared.isInternetConnected {
//            UIApplication.topViewController()?.loadingHide()
//            UIApplication.topViewController()?.checkInternetConnect()
//        }
      //  consoleLog("responseCode  == \(String(describing: responseCode))")

        if responseCode == 440 {
          //  UIApplication.topViewController()?.alertMessageExpire()
        }
    }
    
}

