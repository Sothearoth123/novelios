//
//  SlideTableViewCell.swift
//  MyNovel
//
//  Created by Thy sothearoth on 27/9/21.
//

import UIKit
import FSPagerView

class SlideTableViewCell: UITableViewCell {

    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var containPageControl: UIView!{
        didSet{
            containPageControl.layer.cornerRadius = 8.0
            containPageControl.backgroundColor = .lightText
            containPageControl.alpha = 1.0
        }
    }
    
    @IBOutlet weak var pagerView: FSPagerView!{
        didSet {
            
            self.pagerView.backgroundColor = .clear
            self.pagerView.layer.cornerRadius = 20
            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            
            self.pagerView.delegate = self
            self.pagerView.dataSource = self
            self.pagerView.reloadData()
            self.pagerView.automaticSlidingInterval = 3.5
            self.pagerView.itemSize = FSPagerView.automaticSize
            self.pagerView.decelerationDistance = FSPagerView.automaticDistance
             
            
            //self.pagerView.transformer = FSPagerViewTransformer(type: .linear)
            let transform = CGAffineTransform(scaleX: 0.95, y: 1)
            self.pagerView.itemSize = self.pagerView.frame.size.applying(transform)
            self.pagerView.interitemSpacing = 20
            //self.pagerView.setShadowColor(shadowColor: .clear, viewCorner: 0, shadowRadius: 0)
            
        }
    }
    
    @IBOutlet weak var pageControl:FSPageControl!{
        didSet {
            self.pageControl.hidesForSinglePage = true
        }
    }
    
    @IBOutlet weak var pageControlConstraint: NSLayoutConstraint!
    var isType = ""
    var arrStory: [StoryEntity] = [] {
        didSet {
            consoleLog("arrStorySlide ==> \(arrStory)")
            self.pageControl.numberOfPages = self.arrStory.count
            self.pageControlConstraint.constant = CGFloat(self.arrStory.count * 15)
            pagerView.reloadData()
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
}

extension SlideTableViewCell: FSPagerViewDataSource, FSPagerViewDelegate {
    
    // MARK:- FSPagerViewDataSource
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.arrStory.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        let entity = self.arrStory[index]
        let url = URL(string: entity.img)
        let data = try? Data(contentsOf: url!)
       
        cell.imageView?.image = UIImage(data: data!)
      
        cell.imageView?.contentMode = .scaleToFill
        //cell.imageView?.layer.shadowRadius = 10
        cell.imageView?.layer.cornerRadius = 10.0
        cell.imageView?.layer.shadowColor = UIColor.clear.cgColor
        cell.imageView?.clipsToBounds = true
      
        
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }
    
    // MARK:- FSPagerViewDelegate
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pageControl.currentPage = targetIndex
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        self.pageControl.currentPage = pagerView.currentIndex
    }
    
    
    
}

