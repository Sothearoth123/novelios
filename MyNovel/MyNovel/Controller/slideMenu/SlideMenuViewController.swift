//
//  SlideMenuViewController.swift
//  MyNovel
//
//  Created by Thy sothearoth on 27/9/21.
//

import UIKit

class SlideMenuViewController: BaseViewController {
    static func instantiate() -> SlideMenuViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SlideMenuViewController
        return controller
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    {
        didSet{
            self.tableView.delegate = self
            self.tableView.dataSource = self
        }
    }
    var objectsDict : [Dictionary<String, String>] = [
        ["image": "home_img","name": "Home"],
        ["image": "book_img","name": "All Story"],
        ["image": "author_img","name": "Author"],
        ["image":"about_img","name":"About"]
    ]
    
    var listMenu : [MenuEntity] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(nibCell: SlideMenuTableViewCell.self)
        self.getMenu()
        
    }
    
    private func getMenu(){
        if let listMenu = objectsDict as? [[String: String]] {
            for i in listMenu {
                var entity = MenuEntity()
                entity.image = i["image"] ?? ""
                entity.name = i["name"] ?? ""
                self.listMenu.append(entity)
            }
        }
    }
    
}

extension SlideMenuViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(cell: SlideMenuTableViewCell.self, at: indexPath)
        let entity = self.listMenu[indexPath.row]
        cell.updateCell(entity: entity)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            consoleLog("1")
            let view = SeeMoreViewController.instantiate()
            self.navigationController?.pushViewController(view, animated: true)
        }
    }
    
    
}


