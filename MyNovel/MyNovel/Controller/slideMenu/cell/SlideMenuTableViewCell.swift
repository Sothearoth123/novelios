//
//  SlideMenuTableViewCell.swift
//  MyNovel
//
//  Created by Thy sothearoth on 27/9/21.
//

import UIKit

class SlideMenuTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCell(entity:MenuEntity){
        self.nameLabel.text = entity.name
        self.img.image = UIImage(named: entity.image)
    }
    
}
