//
//  MainView.swift
//  MyNovel
//
//  Created by Thy sothearoth on 27/9/21.
//
import UIKit
protocol MainViewProtocol{
    func gotoDetailViewController()
}

class MainView: UIView {
    
    
    var delegate:MainViewProtocol?
    var collectionView: UICollectionView?
    private let flowLayout = UICollectionViewFlowLayout()
    var isType = ""
    var arrStory :[StoryEntity] = []{
        didSet{
            self.collectionView?.reloadData()
           
        }
    }
    
    required init?(coder aDecoder:NSCoder) {
        super.init(coder:aDecoder)
        setupView()
    }
    
    override init(frame:CGRect) {
        super.init(frame:frame)
        setupView()
    }
    
    private func setupView() {
       
        flowLayout.scrollDirection  = .horizontal
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.minimumLineSpacing = 0
    
        collectionView = UICollectionView(frame: self.bounds,
                                          collectionViewLayout: flowLayout)
        
        collectionView?.register(nibCell: MainCollectionViewCell.self)
        
        collectionView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.backgroundColor = .clear
        collectionView?.contentInset.left = 10
        collectionView?.contentInset.right = 10
       
        addSubview(self.collectionView!)
    }
   

    @objc func didButtonClick(_ sender: UIButton) {
//        self.delegates?.didSelectRow(number: sender.tag, entity: )
    }
    
}

//  MARK:- ColleciotnView
extension MainView: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrStory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
     
        self.delegate?.gotoDetailViewController()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        let cell = collectionView.dequeue(cell: MainCollectionViewCell.self, at: indexPath)
        let entity = self.arrStory[indexPath.row]
        cell.updateCell(entity: entity)
        
        return cell
    }
    
}

// MARK:- CollectionViewDelegateFlowLayout
extension MainView: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150,  height: 170)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets (top: 10, left:0, bottom: 0, right: 0)
    }

}

