//
//  MainCollectionViewCell.swift
//  MyNovel
//
//  Created by Thy sothearoth on 27/9/21.
//

import UIKit

class MainCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var mainView: UIView!{
        didSet{
            mainView.backgroundColor = .white
            mainView.layer.cornerRadius = 10.0
            mainView.layer.shadowColor = UIColor.gray.cgColor
            mainView.layer.shadowOffset = CGSize(width: 0.0, height: 8.0)
            mainView.layer.shadowRadius = 5.0
            mainView.layer.shadowOpacity = 0.4
            
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var img: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func updateCell(entity:StoryEntity){
        self.titleLabel.text = entity.titleText
        let url = URL(string: entity.img)
        let data = try? Data(contentsOf: url!)
       
        self.img.image = UIImage(data: data!)
        
    }

}
