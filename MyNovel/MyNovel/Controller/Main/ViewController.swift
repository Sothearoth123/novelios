//
//  ViewController.swift
//  MyNovel
//
//  Created by Thy sothearoth on 25/9/21.
//

import UIKit
import Alamofire
import SideMenu


class ViewController: UIViewController {

    var arrStory :[StoryEntity] = []
    var isType = ""
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            self.tableView.delegate = self
            self.tableView.dataSource = self
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(nibCell:MainTableViewCell.self)
        self.tableView.register(nibCell: SlideTableViewCell.self)
        self.getStoryAllService()
        self.getSlideService()

    }
    
    private func getStoryAllService (){
        self.loadingShow()
        MainService.getMainService(isType: "All"){(isDone,model) in
            self.loadingHide()
            if isDone{
                self.arrStory = model
                
            }
            self.tableView.reloadData()
            
        }
    }
    
    private func getSlideService (){
        self.loadingShow()
        
        MainService.getMainService(isType: "Slide"){(isDone,model) in
            
            self.loadingHide()
            if isDone{
                self.arrStory = model
                consoleLog("self.arrStorySlide ==> \(self.arrStory)")
            }
            self.tableView.reloadData()
            
        }
    }
    
    private func getCollectionStoryService(){
        self.loadingShow()
        
        MainService.getMainService(isType: "collection"){(isDone,model) in
            
            self.loadingHide()
            if isDone{
                self.arrStory = model
               
            }
            self.tableView.reloadData()
            
        }
    }
    
    private func gotoSeeMoreViewController(){
        consoleLog("tap")
        let view = SeeMoreViewController.instantiate()
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    @objc func clickSeeMore(){
        self.gotoSeeMoreViewController()
        
    }
    
    // MARK: - Navigation Side Menu
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let sideMenuNavigationController = segue.destination as? SideMenuNavigationController else { return }
        sideMenuNavigationController.settings = makeSettings()
    }
    
    private func makeSettings() -> SideMenuSettings{
        var settings = SideMenuSettings()
        settings.allowPushOfSameClassTwice = false
        settings.presentationStyle = .menuSlideIn
        settings.statusBarEndAlpha = 0
        return settings
    }

}

extension ViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeue(cell: SlideTableViewCell.self, at: indexPath)
            cell.arrStory = self.arrStory
            cell.isType = "Slide"
            return cell
        }else{
            let cell = tableView.dequeue(cell: MainTableViewCell.self, at: indexPath)
            cell.seeMoreButton.tag = indexPath.row
            cell.seeMoreButton.addTarget(self, action: #selector(clickSeeMore), for: .touchDown)
            cell.mainView.isType = "collection"
            cell.mainView.arrStory = self.arrStory
            cell.mainView.delegate = self
            
            return cell
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 250
        }
        return 250
    }
   
    
}

extension ViewController : MainViewProtocol{
    func gotoDetailViewController() {
        self.gotoSeeMoreViewController()
        consoleLog("hii")
    }
    
    
}
