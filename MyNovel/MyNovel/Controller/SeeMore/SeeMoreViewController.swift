//
//  SeeMoreViewController.swift
//  MyNovel
//
//  Created by Thy sothearoth on 2/10/21.
//

import UIKit

class SeeMoreViewController: BaseViewController {
    
    static func instantiate() -> SeeMoreViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SeeMoreViewController
        return controller
    }
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            self.tableView.delegate = self
            self.tableView.dataSource = self
        }
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "New Story"
        self.tableView.register(nibCell: MainTableViewCell.self)
       
    }
    

}
extension SeeMoreViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(cell: MainTableViewCell.self, at: indexPath)
        return cell
    }
    
    
}
