//
//  MainService.swift
//  MyNovel
//
//  Created by Thy sothearoth on 25/9/21.
//

import UIKit
import SwiftyJSON
import Alamofire

class MainService: HttpRequest {
    
    class func getMainService (isType:String,block : @escaping (_ isDone: Bool, _ models: [StoryEntity]) -> Void){
        //http://172.20.10.2/stories/detail/1
        //http://172.20.10.2/stories/count/6
        //http://172.20.10.2/popularStories/count/4
        //http://172.20.10.2/popularStories
        var url = ""
        if isType == "Slide"{
            url = "http://172.20.10.2/popularStories/count/4"
        }else if isType == "All"{
            url = "http://127.0.0.1/stories/all"
        }else if isType == "collection"{
            url = "http://172.20.10.2/stories/count/6"
        }else if isType == "popular"{
            url = "http://172.20.10.2/popularStories"
        }
        var report :[StoryEntity] = []
        self.onRequestGET(url: url, param: [:]) { model in

           
            print("jsonData ==> \(model)")
            
            for i in model.arrayValue{
                
                let titleText = i["titleText"].stringValue
                let authorText = i["authorText"].stringValue
                let id = i["id"].stringValue
                let img = i["img"].stringValue
                print("titleText ==> \(titleText)")
                
                let model = StoryEntity(authorText: authorText, id: id, titleText: titleText, img: img)
                
                
                report.append(model)
                
                
            }
            if model.count<1{
                block(false,report)
                return
            }
            
            block(true,report)
            
        }
        
    }
    
    
}
