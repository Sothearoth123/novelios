//
//  BaseViewController.swift
//  MyNovel
//
//  Created by Thy sothearoth on 1/10/21.
//

import UIKit
//import UIScrollView_InfiniteScroll

enum NavigationType {
    case normal
    case none
    case hide
    case show
}

enum NavigationButtonLeftType {
    case normal
    case none
    case close(action: ()->())
    case back(action: ()->())
}

class BaseViewController: UIViewController {

    override var title: String? {
        didSet {
            super.title = title
            self.navigationItem.title = title
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.colorTitle]
        }
    }
    
    var navigationButtonLeftType = NavigationButtonLeftType.normal {
        didSet {
            updateNavigationButtonLeft()
        }
    }
    
    var navigationType = NavigationType.normal {
        didSet {
            updateNavigationType()
        }
    }
    
    private let tagViewInternet = 789
    private let tagLabelInternet = 987
    private var viewMainInternet: UIView?
    private var isModelAnimated = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        listenNotification()
        
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        
        switch navigationButtonLeftType {
        case .normal:
            updateNavigationButtonLeft()
        default:
            break
        }
        
        switch navigationType {
        case .normal:
            updateNavigationType()
        default:
            break
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let className = NSStringFromClass(type(of: self)).components(separatedBy: ".").last!
        consoleLog("ClassName :> \(className)")
    }
    
    private func updateNavigationType() {
        
        switch navigationType {
        case .none:
            setNavigationClear()
        case .normal:
            addNavigationNormal()
        break
        case .hide:
            setHideNaviationBar()
        case .show:
            setShowNaviationBar()
        }
        
    }
    
    func dismissViewController() {
        
        ThreadHelper.runUI {
            self.view.endEditing(true)
        }
        
        if let navigationController = self.navigationController, navigationController.viewControllers.first != self {
            let _ = navigationController.popViewController(animated: isModelAnimated)
            return
        }
        
        if self.presentingViewController != nil {
            self.dismiss(animated: isModelAnimated, completion: nil)
        }
        
    }
    
    private func updateNavigationButtonLeft() {
        
        addButtonMenuNavigation(type: navigationButtonLeftType) {

            switch self.navigationButtonLeftType {
            case .close(action: let closure):
                closure()
            case .normal:
                self.dismissViewController()
            case .none:
                self.navigationItem.leftBarButtonItems = [UIBarButtonItem(customView: UIView())]
            case .back(action: let closure):
                closure()
            }

        }
        
    }
    
    func setNavigationClear() {
        
        if let navigation = navigationController, !navigation.isNavigationBarHidden {
            
            navigation.navigationBar.setBackgroundImage(UIImage(),
                                                        for: UIBarMetrics.default)
            navigation.navigationBar.shadowImage     = UIImage()
            navigation.navigationBar.backgroundColor = UIColor.clear
            navigation.navigationBar.alpha           = 1
            navigation.navigationBar.tintColor       = UIColor.clear
            navigation.navigationBar.isTranslucent   = true
           
            
            
        }
    }
    
    func setHideNaviationBar() {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func setShowNaviationBar() {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    // Listion Internet notificaion
    
    func listenNotification() {
        removeNotification()
        //NotificationCenter.default.addObserver(self,
//                                               selector: #selector(self.reachabilityChanged(_:)),
//                                               name: ReachabilityChangedNotification,
//                                               object: nil)
    }
    
    func removeNotification() {
        //NotificationCenter.default.removeObserver(self,
//                                                  name: ReachabilityChangedNotification,
//                                                  object: nil)
        
        //NotificationCenter.default.removeObserver(self,
//                                                  name: .updateLanguage,
//                                                  object: nil)
    }
    
    @objc func reachabilityChanged(_ notification: Notification) {
        
        updateInternetConnection()
    }
    
    
    
    func updateInternetConnection() {
//        if InternetConnectionManager.shared.isInternetConnected {
//            removeViewInternet()
//        } else {
//            addViewInternet()
//        }
    }
    
    func addViewInternet() {
        
        let coordinateY = self.view.frame.size.height
        viewMainInternet = self.view.viewWithTag(tagViewInternet) ?? {
            let internetStatusLabel = UILabel(frame: CGRect(x: 0,
                                                            y: 0,
                                                            width: screenSize.width,
                                                            height: 35))
            let viewTemp = UIView(frame: CGRect(x: 0,
                                                y: -35,
                                                width: screenSize.width,
                                                height: 35))
            internetStatusLabel.textAlignment = .center
            internetStatusLabel.font = UIFont.systemFont(ofSize: 13)

            internetStatusLabel.tag = self.tagLabelInternet
            internetStatusLabel.backgroundColor = .clear
            internetStatusLabel.alpha = 1
            
           // internetStatusLabel.text = "No Internet Connection"
            viewTemp.tag = self.tagViewInternet
            viewTemp.addSubview(internetStatusLabel)
            self.view.addSubview(viewTemp)
            
            return viewTemp
            
        }()
        
        viewMainInternet?.backgroundColor = UIColor.white
        
        //UIColor(red: 248/255.0,
        //green: 147/255.0,
        //blue: 31/255.0,
        //alpha: 1)
        
        if let internetStatusLabel = self.viewMainInternet?.viewWithTag(self.tagLabelInternet) as? UILabel {
            internetStatusLabel.text = "No Internet Connection"
            internetStatusLabel.textColor = UIColor.red
        }
        
        print("coordinateY ======== \(coordinateY)")
//        ThreadManager.delay(dalay: 0.5) {
//            UIView.animate(withDuration: 0.4, animations: {
//                self.viewMainInternet?.frame.origin.y = 0 //coordinateY - self.viewMainInternet!.frame.size.height
//            })
//        }
    }
    
     func removeViewInternet() {
        
        DispatchQueue.main.async {
            //let coordinateY = self.view.frame.size.height
            if let internetStatusLabel = self.viewMainInternet?.viewWithTag(self.tagLabelInternet) as? UILabel {
                internetStatusLabel.text = "Connected"
                internetStatusLabel.textColor = UIColor.darkGray
            }
            
            self.viewMainInternet?.backgroundColor = UIColor.white
            UIView.animate(withDuration: 0.4,
                           delay: 1,
                           animations: {
                self.viewMainInternet?.frame.origin.y = -35
            })
        }
    }
}

