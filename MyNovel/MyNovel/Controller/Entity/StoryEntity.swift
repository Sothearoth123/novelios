//
//  StoryEntity.swift
//  MyNovel
//
//  Created by Thy sothearoth on 25/9/21.
//

import Foundation

struct StoryEntity {
    
    var authorText = ""
    var id = ""
    var titleText = ""
    var img = ""
}
