//
//  UIViewEX.swift
//  CUSCEN
//
//  Created by MAC on 12/18/20.
//

import UIKit

extension UIView {
    
//    func noDataAvailable(frame: CGRect? = nil, text: String = "No Data Available", isLoad: Bool = false, noInternetAction: (() ->())? = nil) {
//        
//        if let _ = self.viewWithTag(tagView.noData) {
//            
//        } else {
//            removeNoDataAvailable()
//            let noDataView = NoDataAvailableView(frame: frame ?? self.bounds)
//            noDataView.backgroundColor = .clear
//            noDataView.tag = tagView.noData
//            noDataView.labelText.text = text
//            noDataView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//            self.addSubview(noDataView)
//        }
//    }
    
    func removeNoDataAvailable() {
        if let view = self.viewWithTag(tagView.noData) {
            view.removeFromSuperview()
        }
    }
    
    func setShadowColor(shadowColor: UIColor, viewCorner: CGFloat, shadowRadius: CGFloat? = 1) {
            layer.shadowColor = shadowColor.cgColor
            layer.shadowOpacity = 0.5
            layer.shadowRadius = shadowRadius!
            layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            layer.masksToBounds = false
            layer.cornerRadius = viewCorner
            layer.borderColor = UIColor.clear.cgColor
            layer.borderWidth = 0
            layer.backgroundColor = UIColor.clear.cgColor
        }
    
}
