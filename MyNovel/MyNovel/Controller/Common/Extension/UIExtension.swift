//
//  UIExtension.swift
//  MyNovel
//
//  Created by Thy sothearoth on 1/10/21.
//

import Foundation
import SwiftOverlays

var scrollViewKey: UInt8 = 0

extension UITableView {
    
    func loadingShowSkeleton() {
    }
    
    func loadingHideSkeleton() {
        self.removeFromSuperview()
    }
}

extension UIViewController {
    
    func presentFullScreen(controller: UIViewController, complete: (() -> Void)? = nil)  {
        controller.modalPresentationStyle = .fullScreen
        present(controller, animated: false) {
            complete?()
        }
    }
    
    @objc func onCloseHandler() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func onBackHandler() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func addCloseBtn() {
        let item = UIBarButtonItem(image: UIImage(named: "ic_close"),
                                   style: .plain,
                                   target: self,
                                   action: #selector(onCloseHandler))
        item.tintColor = UIColor.blue
        navigationItem.leftBarButtonItem = item
    }
    
    func addBackBtnItem() {
        let item = UIBarButtonItem(image: UIImage(named: "ic_back"),
                                   style: .plain,
                                   target: self,
                                   action: #selector(onBackHandler))
        item.tintColor = UIColor.blue
        navigationItem.leftBarButtonItem = item
        
//        let customeView = UIView(frame: CGRect(x: 0,
//                                               y: 0,
//                                               width: 65,
//                                               height: 35))
//        let button = UIButton(type: UIButton.ButtonType.custom)
//        button.frame = customeView.bounds
//        button.setImage(UIImage(named: "ic_back") , for: .normal)
//        button.tintColor = .black
//        button.imageEdgeInsets = UIEdgeInsets(top: 7, left: 0, bottom: 7, right: 30)
//        button.addTargetClosure { (button) in
//            action()
//        }
//        customeView.addSubview(button)
//
//        navigationItem.leftBarButtonItems = [UIBarButtonItem(customView: customeView)]
    }
    
    func loadingShow() {
        
        let containerViewTag = 456987123
        if let _ = self.view.viewWithTag(containerViewTag) {
            
        } else {
            for subview in self.view.subviews {
                if subview.tag != 9900 {
                    subview.isUserInteractionEnabled = false
                }
            }
            SwiftOverlays.showCenteredWaitOverlay(self.view)
        }
    }
    
    func loadingHide() {
        for subview in self.view.subviews {
            subview.isUserInteractionEnabled = true
        }
        
        SwiftOverlays.removeAllOverlaysFromView(self.view)
    }
    
    func alertMessageExpire() {
        
        self.showAlertMessage(message: "Session was expired.", performAction : {
           // Launcher.launchCompany()
        })
        
    }
    
    func checkInternetConnect() {
        
        self.showAlertMessage(message: "Please check your Internet connection", performAction : {
            
        })
        
    }
    
    func setSourceViewFromBarItem(_ sourceView: UIBarButtonItem) {
        if let popoverController = self.popoverPresentationController {
            popoverController.barButtonItem =  sourceView
        }
    }
    
    func setContentAlert(controller: UIViewController, height: CGFloat) {
        setValue(controller, forKey: "contentViewController")
        controller.preferredContentSize = CGSize(width: screenBound.size.width - 40, height: height)
    }
    
    //Set background color of UIAlertController
    func setBackgroundColor(color: UIColor) {
        if let bgView = self.view.subviews.first, let groupView = bgView.subviews.first, let contentView = groupView.subviews.first {
            contentView.backgroundColor = color
        }
    }
    
    func showAlertMessage(_ title:String = "Phsartepnimet", message: String,_ messageButton: String = "OK",messageCancel:String = "", performAction: (() -> ())? = nil) {
        
        let alertController = UIAlertController(title: title, message:
                                                    message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: messageButton, style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
            if let action = performAction{
                action()
            }
        }))
        
        if !messageCancel.isEmpty  {
            alertController.addAction(UIAlertAction(title: messageCancel, style: .default, handler: { action in }))
        }
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func addButtonMenuNavigation(type: NavigationButtonLeftType,
                                 action: @escaping ()->()) {
        
        
        switch type {
        case .none:
            navigationItem.leftBarButtonItems = [UIBarButtonItem(customView: UIView())]
            
        case .close(_):
            
            let customeView = UIView(frame: CGRect(x: 0,
                                                   y: -10,
                                                   width: 25,
                                                   height: 30))
            
            let button = UIButton(type: UIButton.ButtonType.custom)
            button.frame = customeView.bounds
            button.setImage(UIImage(named: "ic_close") , for: .normal)
            button.tintColor = UIColor.black
            button.imageEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 10)
            button.addTargetClosure { (button) in
                action()
            }
            
            customeView.addSubview(button)
            navigationItem.leftBarButtonItems = [UIBarButtonItem(customView: customeView)]
            
        case .normal, .back(_):
            
            if let navigationController = self.navigationController, navigationController.viewControllers.first != self {
                
                let customeView = UIView(frame: CGRect(x: 0,
                                                       y: 0,
                                                       width: 45,
                                                       height: 30))
                let button = UIButton(type: UIButton.ButtonType.custom)
                button.frame = customeView.bounds
                button.setImage(UIImage(named: "ic_back") , for: .normal)
                button.tintColor = .blue
                button.imageEdgeInsets = UIEdgeInsets(top: 7, left: 0, bottom: 7, right: 30)
                button.addTargetClosure { (button) in
                    action()
                }
                customeView.addSubview(button)
                
                navigationItem.leftBarButtonItems = [UIBarButtonItem(customView: customeView)]
                
            } else {
                
                let customeView = UIView(frame: CGRect(x: 3,
                                                       y: 0,
                                                       width: 25,
                                                       height: 35))
                let button = UIButton(type: UIButton.ButtonType.custom)
                button.frame = customeView.bounds
                button.setImage(UIImage(named: "ic_close") , for: .normal)
                button.tintColor = UIColor.black
                button.imageEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 10)
                button.addTargetClosure { (button) in
                    action()
                }
                customeView.addSubview(button)
                navigationItem.leftBarButtonItems = [UIBarButtonItem(customView: customeView)]
                
            }
        }
    }
    
    func removeNavigationBottomBorder() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
    }
    
    func resetNavigationBottomBorder() {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for:.default)
        self.navigationController?.navigationBar.shadowImage = nil
        self.navigationController?.navigationBar.layoutIfNeeded()
    }
    
    func addNavigationNormal() {
        
        navigationController?.navigationBar.setBackgroundImage(nil,
                                                               for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage     =  nil //UIImage()
        navigationController?.navigationBar.backgroundColor = UIColor.white
        navigationController?.navigationBar.alpha           = 1
        navigationController?.navigationBar.tintColor       = UIColor.white
        navigationController?.navigationBar.isTranslucent   = false
        
    }
    
    public func setupKeyboardNotifcationListenerForScrollView(_ scrollView: UIScrollView) {
        NotificationCenter.default.addObserver(self, selector: #selector(UIViewController.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(UIViewController.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        internalScrollView = scrollView
    }
    
    public func removeKeyboardNotificationListeners() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    fileprivate var internalScrollView: UIScrollView! {
        get {
            return objc_getAssociatedObject(self, &scrollViewKey) as? UIScrollView
        }
        set(newValue) {
            objc_setAssociatedObject(self, &scrollViewKey, newValue, .OBJC_ASSOCIATION_ASSIGN)
        }
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo = (notification as NSNotification).userInfo as! Dictionary<String, AnyObject>
        let animationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! TimeInterval
        let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey]?.cgRectValue
        let keyboardFrameConvertedToViewFrame = view.convert(keyboardFrame!, from: nil)
        let options = UIView.AnimationOptions.beginFromCurrentState
        UIView.animate(withDuration: animationDuration, delay: 0, options:options, animations: { () -> Void in
            let insetHeight = (self.internalScrollView.frame.height + self.internalScrollView.frame.origin.y) - keyboardFrameConvertedToViewFrame.origin.y
            self.internalScrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: insetHeight + 25, right: 0)
            self.internalScrollView.scrollIndicatorInsets  = UIEdgeInsets(top: 0, left: 0, bottom: insetHeight + 145, right: 0)
        }) { (complete) -> Void in
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        let userInfo = (notification as NSNotification).userInfo as! Dictionary<String, AnyObject>
        let animationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! TimeInterval
        let options = UIView.AnimationOptions.beginFromCurrentState
        UIView.animate(withDuration: animationDuration, delay: 0, options:options, animations: { () -> Void in
            self.internalScrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            self.internalScrollView.scrollIndicatorInsets  = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }) { (complete) -> Void in
            
        }
    }
    
    
}

