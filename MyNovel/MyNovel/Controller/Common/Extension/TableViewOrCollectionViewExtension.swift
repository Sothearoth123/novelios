//
//  TableViewOrCollectionViewExtension.swift
//  CUSCEN
//
//  Created by MAC on 10/1/20.
//

import UIKit

//MARK: - TableView
extension UITableView {
    func register<T: UITableViewCell>(nibCell _: T.Type) {
        register(T.nib(), forCellReuseIdentifier: T.reuseIdentifier())
    }
    
    func dequeue<T: UITableViewCell>(cell _: T.Type, at indexPath: IndexPath) -> T {
        return dequeueReusableCell(withIdentifier: T.reuseIdentifier(), for: indexPath) as! T
    }
    
    func lastIndexpath() -> IndexPath {
        let section = max(numberOfSections - 1, 0)
        let row = max(numberOfRows(inSection: section) - 1, 0)
        return IndexPath(row: row, section: section)
    }
}

extension UITableViewCell {
    class func reuseIdentifier() -> String {
        return "\(self)"
    }
    
    class func nib() -> UINib {
        return UINib(nibName: reuseIdentifier(), bundle: nil)
    }
}


//MARK: - CollectionView
extension UICollectionView {
    
    func register<T: UICollectionViewCell>(nibCell: T.Type) {
        register(T.nib(), forCellWithReuseIdentifier: T.reuseIdentifier())
    }
    
    func dequeue<T: UICollectionViewCell>(cell: T.Type, at indexPath: IndexPath) -> T {
        return dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier(), for: indexPath) as! T
    }
    
    func lastIndexpath() -> IndexPath? {
        let numberOfSection = numberOfSections
        if numberOfSection == 0 {
            return nil
        }
        let section = max(numberOfSections - 1, 0)
        let item = numberOfItems(inSection: section)
        let row = max(item - 1, 0)
        let indexPath = IndexPath(item: row, section: section)
        return indexPath
    }
}

extension UICollectionViewCell {
    class func reuseIdentifier() -> String {
        return "\(self)"
    }
    
    class func nib() -> UINib {
        return UINib(nibName: reuseIdentifier(), bundle: nil)
    }
}



