//
//  UIApplicationEx.swift
//  CUSCEN
//
//  Created by MAC on 10/16/20.
//

import Foundation
import UIKit

extension UIApplication {
    
    static var appDelegate: AppDelegate {
        return shared.delegate as! AppDelegate
    }
    
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        
        return controller
    }
    
}

