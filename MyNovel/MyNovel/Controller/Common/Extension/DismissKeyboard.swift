//
//  DismissKeyboard.swift
//  CUSCEN
//
//  Created by MocOS on 10/12/20.
//

import Foundation
import UIKit

extension UIViewController{
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dimissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dimissKeyboard() {
        view.endEditing(true)
    }
}
