//
//  VariableGloble.swift
//  CUSCEN
//
//  Created by MAC on 10/1/20.
//

import UIKit

let googleAPIKey = "AIzaSyCAdjOVibdDA-r3sES1zAECFoMl3iXeSHo"
let limitMessage =  10

//var languageApp = UserDefaults.standard.string(forKey: KeyCache.language_app) ?? "EN" {
//    didSet {
//        UserDefaults.standard.set(languageApp, forKey: KeyCache.language_app)
////        NotificationCenter.default.post(name: .updateLanguage, object: nil)
//    }
//}

let isDeviceIPad = UIDevice.current.userInterfaceIdiom == .pad ? true : false
let iSIPhone5Series = UIScreen.main.nativeBounds.height == 1136 ? true : false
let screenBound = UIScreen.main.bounds

@IBDesignable class MainDesginView :UIView {

    @IBInspectable var radius :CGFloat = 10
    @IBInspectable var borderWhite: CGFloat = 0.1

    override func draw(_ rect: CGRect) {
        layer.cornerRadius = radius
        layer.borderWidth = borderWhite

    }

}

extension Int {
    var isZero: Bool {
        return self == 0 ? true:false
    }
    
    var isDoubleValue: Double {
      return Double(self)
    }
}

struct tagView {
    
    static let noData = 121212
}

struct KeyCache {

    static let language_app = "language_app"
    static let accessToken = "accessToken"
    static let profile = "profile"
    
}

struct Defaults {
    static var language: String? {
        get {
            let language = UserDefaults.standard.string(forKey: KeyCache.language_app)
            return language ?? nil
        }
        set {
            UserDefaults.standard.set(newValue, forKey: KeyCache.language_app)
            UserDefaults.standard.synchronize()
        }
    }
}

import UIKit

extension Notification.Name {
    static let updateLanguage = Notification.Name("updateLanguage")
}

extension String {
    var isBlank: Bool {
        get {
            let trimmed = trimmingCharacters(in: CharacterSet.whitespaces)
            return trimmed.isEmpty
        }
    }
    
    var appLanguage: String {
        get {
            if let language = Defaults.language {
                
                return language
            }else{
                Defaults.language = "en"
                return "en"
            }
        }
        
        set(value) {
            Defaults.language = value
        }
    }
    
    var localized: String {
        return localized(lang: appLanguage)
    }
    
    var localizeStringUsingSystemLang: String {
        return NSLocalizedString(self, comment: "")
    }
    
    func localized(lang: String?) -> String {
        
        if let lang = lang {
            if let path = Bundle.main.path(forResource: lang, ofType: "lproj") {
                let bundle = Bundle(path: path)
                //consoleLog("lang = \(lang) == \(path)")
                return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
            }
        }
        
        return localizeStringUsingSystemLang
    }
    
}

//func convertDoubleToCurrency(amount: Double) -> String{
//    let numberFormatter = NumberFormatter()
//
//    numberFormatter.locale = Locale.current
//    return numberFormatter.string(from: NSNumber(value: amount))!
//}

//func convertCurrencyToDouble(input: String) -> Double? {
//     let numberFormatter = NumberFormatter()
//     numberFormatter.numberStyle = .currency
//     numberFormatter.locale = Locale.current
//     return numberFormatter.number(from: input)?.doubleValue
//}


func convertDoubleToCurrency(amount: Double) -> String{
    let numberFormatter = NumberFormatter()
    
    numberFormatter.usesGroupingSeparator = true
    numberFormatter.numberStyle = .currency
    // localize to your grouping and decimal separator
    numberFormatter.locale = Locale(identifier: "en_US")
    numberFormatter.maximumFractionDigits = 5
    return numberFormatter.string(from: NSNumber(value: amount))!
}

func convertFormatCurrency(_ price: Double) -> String {
    let numberFormatter = NumberFormatter()
    numberFormatter.groupingSeparator = ","
    numberFormatter.groupingSize = 3
    numberFormatter.usesGroupingSeparator = true
    numberFormatter.decimalSeparator = "."
    numberFormatter.numberStyle = .decimal
    numberFormatter.maximumFractionDigits = 5
    numberFormatter.minimumSignificantDigits = 5
    
    
    return numberFormatter.string(from: 222000 as NSNumber)!
}

func df2so(_ price: Double) -> String {
    
    let numberFormatter = NumberFormatter()
    numberFormatter.groupingSeparator = ","
    numberFormatter.groupingSize = 2
    numberFormatter.usesGroupingSeparator = true
    numberFormatter.decimalSeparator = ","
    numberFormatter.numberStyle = .decimal
    numberFormatter.maximumFractionDigits = 3
    
    return numberFormatter.string(from: price as NSNumber)!
}


func removeFormatAmount(string: String) -> NSNumber {
    let formatter = NumberFormatter()
    formatter.numberStyle = .none
    formatter.currencySymbol = .none
    formatter.currencyGroupingSeparator = .none
    return formatter.number(from: string)!
}

let timeZoneName = NSTimeZone.local.identifier
#if targetEnvironment(simulator)
    let isSimulator = true
    let device_id   = "3B3AED3C-A2D6-4614-8E61-8EBD15CDEE94"
#else
    let isSimulator = false
    let device_id = UIDevice.current.identifierForVendor?.uuidString
#endif



