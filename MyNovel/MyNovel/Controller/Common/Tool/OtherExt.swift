//
//  OtherExt.swift
//  DGO Store
//
//  Created by Sovannarith on 2/23/20.
//  Copyright © 2020 NEXVIS SOLUTION. All rights reserved.
//

import UIKit


//MARK: - Double
extension Double {
    func getKhmerRielPriceFormat() -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currencyISOCode
        formatter.locale = Locale(identifier: "km_KH")
        formatter.usesGroupingSeparator = true
        let priceStr = formatter.string(from: NSNumber(value: self))
        return (priceStr?.replacingOccurrences(of: "KHR", with: "") ?? "")
    }

}


//MARK: - String
extension String {
    
    static let numberFormatter = NumberFormatter()
    var doubleValue: Double {
        String.numberFormatter.decimalSeparator = "."
        if let result =  String.numberFormatter.number(from: self) {
            return result.doubleValue
        } else {
            String.numberFormatter.decimalSeparator = ","
            if let result = String.numberFormatter.number(from: self) {
                return result.doubleValue
            }
        }
        return 0
    }
    
    func generateQRCode() -> UIImage? {
        let data = self.data(using: String.Encoding.ascii)
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 5, y: 5)
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
    
    func getInitialName() -> String {
        if self.isBlank { return "" }
        let splitNameArray = self.split(separator: " ")
        let first = String(splitNameArray[0].first!).capitalized
        if splitNameArray.count == 1 {
            return first
        } else {
            let second = String(splitNameArray[1].first!).capitalized
            return first + second
        }
    }
    
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    var isValidPhone: Bool {
        let phoneRegex = "^[0-9+]{0,1}+[0-9]{5,16}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        return phoneTest.evaluate(with: self)
    }
    
    func getDateFromString(formatString: String) -> Date? {
        let format = DateFormatter()
        format.dateFormat = formatString
        let date = format.date(from: self)
        return date
    }
    
    func hexStringToUIColor() -> UIColor {
        var cString:String = self.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

//MARK:  - UIView
extension UIView {
    
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    func setRoundCorner(radius: CGFloat, bColor: UIColor? = .clear, bW: CGFloat? = 0) {
        layer.cornerRadius = radius
        layer.borderWidth = bW!
        layer.masksToBounds = true
        layer.borderColor = bColor!.cgColor
    }
    
    func drawCornerBy(isSender: Bool) {
        //        layerMaxXMaxYCorner - bottom right corner
        //        layerMaxXMinYCorner - top right corner
        //        layerMinXMaxYCorner - bottom left corner
        //        layerMinXMinYCorner - top left corner
        if isSender {
            layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner]
        } else {
            layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        }
    }
}

//MARK: - Label
extension UILabel {
    func setMargins(margin: CGFloat = 10) {
        if let textString = self.text {
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.firstLineHeadIndent = margin
            paragraphStyle.alignment = .right
            paragraphStyle.headIndent = margin
            paragraphStyle.tailIndent = -margin
            let attributedString = NSMutableAttributedString(string: textString)
            attributedString.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
            attributedText = attributedString
        }
    }
}

//MARK: - Extra - Helper
func getStringValue(from dictionary: [String: Any]) -> String {
    let data = try! JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
    let jsonStr = String(data: data, encoding: .utf8)
    return jsonStr!
}

func rearrange<T>(array: Array<T>, fromIndex: Int, toIndex: Int) -> Array<T> {
    var arr = array
    let element = arr.remove(at: fromIndex)
    arr.insert(element, at: toIndex)
    return arr
}

//MARK: - Date
extension Date {
    
    var timeStampValue: Int  {
        return Int(self.timeIntervalSince1970)
    }
    
    func iSODateString() -> String {
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let str = format.string(from: self)
        return str
    }
    
    func stingFullDate() -> String {
        let format = DateFormatter()
        format.dateFormat = "E dd MMM, yyyy"
        let dateStr = format.string(from: self)
        return dateStr
    }
    
    func numberOfDaysUntilDateTime(toDateTime: Date) -> Int {
        let calendar = Calendar.current
        let fromDate = calendar.startOfDay(for: self)
        let toDate = calendar.startOfDay(for: toDateTime)
        let difference = calendar.dateComponents([.day], from: fromDate, to: toDate)
        return difference.day!
    }
        
    func monthOfYear() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM"
        return dateFormatter.string(from: self).capitalized
    }
    
    func getCurrentYear() -> Int {
        let dateComponent = Calendar.current.dateComponents([.year], from: self)
        return dateComponent.year!
    }
    
    //1
    func dayNumberOfWeek() -> Int {
        return Calendar.current.dateComponents([.day], from: self).day!
    }
    
    func getLastWeek() -> Date {
        let lastWeekDate = Calendar.current.date(byAdding: .weekOfYear, value: -1, to: self)!
        return lastWeekDate
    }
    
    //Monday
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self)
    }
    
    var isToday: Bool {
        let calendar = Calendar.current
        return calendar.isDateInToday(self)
    }
    
    var isYesterday: Bool {
        let calendar = Calendar.current
        return calendar.isDateInYesterday(self)
    }
    
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: Date().noon)!
    }
    
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: Date().noon)!
    }
    
    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    
    var addOneHour: Date {
        return Calendar.current.date(byAdding: .hour, value: 1, to: self)!
    }
    
    func timeAgoDisplay() -> String {
        let secondsAgo = Int(Date().timeIntervalSince(self))
        let minute = 60
        let hour = 60 * minute
        let day = 24 * hour
        let week = 7 * day
        let month = 4 * week
        let year = 12 * month
        
        if self.isToday {
            if secondsAgo < minute {
                if secondsAgo < 2{
                    return "now"
                }else{
                    return "\(secondsAgo)s"
                }
            } else if secondsAgo < hour {
                let min = secondsAgo/minute
                if min == 1 {
                    return "\(min)m"
                } else{
                    return "\(min)m"
                }
            } else if secondsAgo < day {
                let hr = secondsAgo/hour
                if hr == 1 {
                    return "\(hr)h"
                } else {
                    return "\(hr)h"
                }
            } else {
                return stringWithFormat(str: "h:mm a").lowercased()
            }
        } else {
            if secondsAgo < week {
                var d = secondsAgo/day
                if d == 0 { d = 1 }
                return "\(d)d"
            } else if secondsAgo < month {
                var week = secondsAgo/week
                if week == 0 { week = 1 }
                return "\(week)w"
            } else if secondsAgo < year {
                var month = secondsAgo/month
                if month == 0 { month = 1 }
                return "\(month)mon"
            }
            return "\(secondsAgo / year)y"
        }
    }
    
    static func today() -> Date {
        return Date()
    }
    
    func next(_ weekday: Weekday, considerToday: Bool = false) -> Date {
        return get(.next,
                   weekday,
                   considerToday: considerToday)
    }
    
    func previous(_ weekday: Weekday, considerToday: Bool = false) -> Date {
        return get(.previous,
                   weekday,
                   considerToday: considerToday)
    }
    
    func get(_ direction: SearchDirection,
             _ weekDay: Weekday,
             considerToday consider: Bool = false) -> Date {
        
        let dayName = weekDay.rawValue
        let weekdaysName = getWeekDaysInEnglish().map { $0.lowercased() }
        assert(weekdaysName.contains(dayName), "weekday symbol should be in form \(weekdaysName)")
        let searchWeekdayIndex = weekdaysName.firstIndex(of: dayName)! + 1
        let calendar = Calendar(identifier: .gregorian)
        if consider && calendar.component(.weekday, from: self) == searchWeekdayIndex {
            return self
        }
        var nextDateComponent = calendar.dateComponents([.hour, .minute, .second], from: self)
        nextDateComponent.weekday = searchWeekdayIndex
        
        let date = calendar.nextDate(after: self,
                                     matching: nextDateComponent,
                                     matchingPolicy: .nextTime,
                                     direction: direction.calendarSearchDirection)
        
        return date!
    }
    
    func getWeekDaysInEnglish() -> [String] {
        var calendar = Calendar(identifier: .gregorian)
        calendar.locale = Locale(identifier: "en_US_POSIX")
        return calendar.weekdaySymbols
    }
    
    enum Weekday: String {
        case monday, tuesday, wednesday, thursday, friday, saturday, sunday
    }
    
    enum SearchDirection {
        case next
        case previous
        
        var calendarSearchDirection: Calendar.SearchDirection {
            switch self {
                case .next:
                    return .forward
                case .previous:
                    return .backward
            }
        }
    }
    
    func getWeekDates() -> (thisWeek:[Date],nextWeek:[Date]) {
        var tuple: (thisWeek:[Date],nextWeek:[Date])
        var arrThisWeek: [Date] = []
        for i in 0..<7 {
            arrThisWeek.append(Calendar.current.date(byAdding: .day, value: i, to: startOfWeek)!)
        }
        var arrNextWeek: [Date] = []
        for i in 1...7 {
            arrNextWeek.append(Calendar.current.date(byAdding: .day, value: i, to: arrThisWeek.last!)!)
        }
        tuple = (thisWeek: arrThisWeek,nextWeek: arrNextWeek)
        return tuple
    }
    
    var startOfWeek: Date {
        let gregorian = Calendar(identifier: .gregorian)
        let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))
        return gregorian.date(byAdding: .day, value: 1, to: sunday!)!
    }
    
    var startOfMonth: Date {
        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents([.year, .month], from: self)
        return  calendar.date(from: components)!
    }
    
    var endOfMonth: Date {
        var components = DateComponents()
        components.month = 1
        components.second = -1
        return Calendar(identifier: .gregorian).date(byAdding: components, to: startOfMonth)!
    }
    
    var startOfYear: Date {
        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents([.year], from: self)
        return  calendar.date(from: components)!
    }
    
    var endOfYear: Date {
        var components = DateComponents()
        components.year = 1
        components.second = -1
        return Calendar(identifier: .gregorian).date(byAdding: components, to: startOfYear)!
    }
}
