//
//  Common.swift
//  MyNovel
//
//  Created by Thy sothearoth on 1/10/21.
//


import UIKit

var screenSize      = UIScreen.main.bounds
let isiPhone4       = UIScreen.main.bounds.height == 480 ? true : false
let isiPhone5       = UIScreen.main.bounds.height == 568 ? true : false
let isiPhone6       = UIScreen.main.bounds.height == 667 ? true : false
let isiPhone6Plus   = UIScreen.main.bounds.height == 736 ? true : false
let isiPhoneX       = UIScreen.main.bounds.height >= 812 ? true : false

let HelveticaNeueBold   = "HelveticaNeue-Bold"

func consoleLog(_ text: String) {
    print(text)
}

@IBDesignable class RoundView: UIView {
    
    @IBInspectable var borderWidth: CGFloat = 0
    @IBInspectable var radius: CGFloat = 0
    @IBInspectable var borderColor: UIColor = .clear
       
    override func draw(_ rect: CGRect) {
        layer.cornerRadius = radius
        layer.masksToBounds = true
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
    }
}

class Common {
    
    static func getIPAddress() -> String? {
        var address: String?
        var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next }
                
                let interface = ptr?.pointee
                let addrFamily = interface?.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                    let name: String = String(cString: (interface?.ifa_name)!)
                    if name == "en0" {
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface?.ifa_addr, socklen_t((interface?.ifa_addr.pointee.sa_len)!), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                        address = String(cString: hostname)
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        return address
    }
    
    //MARK: Translate Language
    
//    static func Localize(key: String, text: String) -> String {
//        return NSLocalizedString(key, comment: "")
//    }
//
//    static func L(key: String, text: String? = nil) -> String {
//
//        let realm = try! Realm()
//        let language = realm.objects(LanguageRealm.self).filter("baseText == %@ AND code == %@", key.lowercased(), languageApp)
//
//        var translateResult = language.first?.text ?? (text ?? key)
//
//        if language.first?.text == nil || (language.first?.text ?? "").isEmpty {
//            translateResult = (text ?? key)
//        }
//
//        return translateResult.isEmpty ? text ?? "":translateResult
//    }
}

extension Date {
    
    static var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: Date().noon)!
    }
    
    static var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: Date().noon)!
    }

    
    var isLastDayOfMonth: Bool {
        return dayAfter.month != month
    }
    
    func convertTo24HourFormat() -> Date {
        let dateString = self.stringWithFormat(str: "yyyy-MM-dd HH:mm:ss")
        let format = DateFormatter()
        format.dateFormat = "YYYY/MM/dd HH:mm"
        let newDate = format.date(from: dateString)
        return newDate!
    }
    
    func convertDateFormat() -> Date {
        let dateString = self.stringWithFormat(str: "yyyy-MM-ddyHH:mm:ss")
        let format = DateFormatter()
        format.dateFormat = "dd-mmm-YYYY"
        let newDate = format.date(from: dateString)
        return newDate!
    }
    
    
    func stringWithFormat(str: String) -> String {
        let format = DateFormatter()
        format.dateFormat = str
        let dateStr = format.string(from: self)
        return dateStr
    }
    
    
    func shortDate(format: String) -> Date? {
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = format
            return dateFormatterGet.date(from: format)
        }
    
}

extension String {
    func dateFromString(format: String) -> String {
        let dateFormatterGet = DateFormatter()
        
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = format
        var date: Date? = dateFormatterGet.date(from: self)
        if date == nil { date = Date()}
        return dateFormatterPrint.string(from: date!)
    }
    
    func approveDateFromString(format: String) -> String {
        let dateFormatterGet = DateFormatter()
        
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = format
        var date: Date? = dateFormatterGet.date(from: self)
        if date == nil { date = Date()}
        return dateFormatterPrint.string(from: date!)
    }
    //yyyy-MM-dd HH:mm:ss.SSS
   
}

public extension UIDevice {
    
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod touch (5th generation)"
            case "iPod7,1":                                 return "iPod touch (6th generation)"
            case "iPod9,1":                                 return "iPod touch (7th generation)"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "iPhone12,1":                              return "iPhone 11"
            case "iPhone12,3":                              return "iPhone 11 Pro"
            case "iPhone12,5":                              return "iPhone 11 Pro Max"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad (3rd generation)"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad (4th generation)"
            case "iPad6,11", "iPad6,12":                    return "iPad (5th generation)"
            case "iPad7,5", "iPad7,6":                      return "iPad (6th generation)"
            case "iPad7,11", "iPad7,12":                    return "iPad (7th generation)"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad11,4", "iPad11,5":                    return "iPad Air (3rd generation)"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad mini 4"
            case "iPad11,1", "iPad11,2":                    return "iPad mini (5th generation)"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch)"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }
        
        return mapToDevice(identifier: identifier)
    }()
    
}
