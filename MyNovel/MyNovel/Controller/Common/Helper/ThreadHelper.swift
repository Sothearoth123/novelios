//
//  ThreadHelper.swift
//  CUSCEN
//
//  Created by MAC on 10/1/20.
//

import UIKit

class ThreadHelper {
    
     static var heightRowBrandList = ( (screenSize.size.width - 24) * 175 / 375)
    
    //    static let sharedInstance = ThreadHelper()
    static func delay(dalay: Double,handler: @escaping (() -> ())) {
        DispatchQueue.main.asyncAfter(deadline: .now() + dalay) {
            handler()
        }
    }
    
    static func runUI(updateUI: @escaping (()->Void)) {
        DispatchQueue.main.async {
            updateUI()
        }
    }
    
    static func runBackground(doBackground: @escaping (()->Void), updateUI: (()->Void)? = nil) {
        DispatchQueue.global(qos: .background).async {
            doBackground()
            DispatchQueue.main.async {
                updateUI?()
            }
        }
    }
}


